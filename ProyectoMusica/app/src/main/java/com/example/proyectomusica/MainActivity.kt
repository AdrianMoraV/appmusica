package com.example.proyectomusica

import PermissionsUtil
import PermissionsUtil.requestPermissions
import android.Manifest
import android.content.ContentUris
import android.content.Intent
import android.database.Cursor
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity() {
    private val STORAGE_PERMISSION_ID = 0
    lateinit var mediaPlayer: MediaPlayer
    private lateinit var runnable: Runnable
    private var handler: Handler = Handler()
    private var pause: Boolean = false
    private var cancionActualIndex =0
    private var songList = ArrayList<Song>()
    private val sArtworkUri = Uri.parse("content://media/external/audio/albumart")
    private val adapter by lazy {
        SongAdapter { song, pos ->
            playMusic(song.songLink,song.title,song.artist,song.thumbnail)
            cancionActualIndex=pos
            //Toast.makeText(this, "pos: ${pos}", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        imgPause.visibility = View.INVISIBLE
        //var songList = mutableListOf<Song>()
        init()
        mediaPlayer = MediaPlayer()
        fun getSongList() {
            val musicResolver = contentResolver
            val musicUri: Uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
            val musicCursor: Cursor? = musicResolver.query(musicUri, null, null, null, null)
            if (musicCursor != null && musicCursor.moveToFirst()) {
                //get columns
                val titleColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media.TITLE)
                val idColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media._ID)
                val artistColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST)
                val songLink = musicCursor.getColumnIndex((MediaStore.Audio.Media.DATA))
                val albumID = musicCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID)
                //add songs to list
                do {
                    val thisId = musicCursor.getLong(idColumn)
                    val thisTitle = musicCursor.getString(titleColumn)
                    val thisArtist = musicCursor.getString(artistColumn)
                    val thisLink = musicCursor.getString(songLink)
                    val some = musicCursor.getLong(albumID)
                    val uri = ContentUris.withAppendedId(sArtworkUri, some)
                    songList.add(Song(thisId, thisTitle, thisArtist, thisLink,uri.toString()))
                } while (musicCursor.moveToNext())
            }
            Toast.makeText(this, "${songList.size} Songs Found!!!", Toast.LENGTH_SHORT).show();
        }

        getSongList()
        rvSongs.adapter = adapter
        adapter.setList(songList)
        imgPlay.setOnClickListener {
            mediaPlayer.start()
            pause = false
            imgPlay.visibility = View.INVISIBLE
            imgPause.visibility = View.VISIBLE
        }

        imgPause.setOnClickListener {
            if (mediaPlayer.isPlaying) {
                mediaPlayer.pause()
                pause = true

                Toast.makeText(this, "media pause", Toast.LENGTH_SHORT).show()
                imgPause.visibility = View.INVISIBLE
                imgPlay.visibility = View.VISIBLE
            }
        }
        layoutSong.setOnClickListener{
            val intent = Intent(this, SongPlaying::class.java).apply {
                /*val bundle = Bundle()
                bundle.putParcelableArrayList("array", songList)
                bundle.putInt("cancionActual",cancionActualIndex)*/
                putExtra("ARRAY", songList)
                //intent.putParcelableArrayListExtra("ARRAY", songList)
                putExtra("CANCIONACTUAL", cancionActualIndex)
                putExtra("PROGRESO", mediaPlayer.currentSeconds)
            }
            startActivity(intent)
            if (mediaPlayer.isPlaying) {
                mediaPlayer.pause()
                pause = true

                Toast.makeText(this, "media pause", Toast.LENGTH_SHORT).show()
                imgPause.visibility = View.INVISIBLE
                imgPlay.visibility = View.VISIBLE
            }
        }
        /*imgNext.setOnClickListener {
            playNext(cancionActualIndex)
        }
        imgAnterior.setOnClickListener {
            playAnterior(cancionActualIndex)
        }*/

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                if (b) {
                    mediaPlayer.seekTo(i * 1000)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
            }
        })
    }

     fun playMusic(ID: String?, title: String?, artist: String?, thumbnail: String?) {
        if (pause) {
            mediaPlayer.seekTo(mediaPlayer.currentPosition)
            mediaPlayer.start()
            pause = false
            //Toast.makeText(this, "media playing", Toast.LENGTH_SHORT).show()
            imgPlay.visibility = View.INVISIBLE
            imgPause.visibility = View.VISIBLE
            //imgThumbnailCurrent.setImageURI(Uri.parse(thumbnail))
        } else {
            mediaPlayer.reset()
            mediaPlayer.apply {
                setDataSource(applicationContext, Uri.parse(ID))
                prepare()
                start()
            }
            /*mediaPlayer =
                MediaPlayer.create(applicationContext, ID)
            mediaPlayer.start()*/
            //Toast.makeText(this, "media playing $ID", Toast.LENGTH_SHORT).show()
            imgPlay.visibility = View.INVISIBLE
            imgPause.visibility = View.VISIBLE
            tvNombre.text = "${title}"
            tvArtista3.text="${artist}"
            imgThumbnailCurrent.setImageURI(Uri.parse(thumbnail))
        }
        initializeSeekBar()


        mediaPlayer.setOnCompletionListener {
            Toast.makeText(this, "end", Toast.LENGTH_SHORT).show()
        }

    }/*
    fun playNext(pos: Int){
        if(pos == songList.size-1){
            var cancion: Song = songList.get(0)
            playMusic(cancion.songLink,cancion.title,cancion.artist,cancion.thumbnail)
            cancionActualIndex=0
        }else{
            var cancion: Song = songList.get(pos+1)
            playMusic(cancion.songLink,cancion.title,cancion.artist,cancion.thumbnail)
            cancionActualIndex=pos+1
        }

    }

    fun playAnterior(pos: Int){
        if(pos == 0){
            var cancion: Song = songList.get(songList.size-1)
            playMusic(cancion.songLink,cancion.title,cancion.artist,cancion.thumbnail)
            cancionActualIndex=songList.size-1
        }else{
            var cancion: Song = songList.get(pos-1)
            playMusic(cancion.songLink,cancion.title,cancion.artist,cancion.thumbnail)
            cancionActualIndex=pos-1
        }

    }*/

    private fun init() {
        if (!checkStorePermission(STORAGE_PERMISSION_ID)) {
            showRequestPermission(STORAGE_PERMISSION_ID)
        }
    }

    private fun checkStorePermission(permission: Int): Boolean {
        return if (permission == STORAGE_PERMISSION_ID) {
            PermissionsUtil.checkPermissions(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
        } else {
            true
        }
    }

    private fun showRequestPermission(requestCode: Int) {
        val permissions: Array<String> = if (requestCode == STORAGE_PERMISSION_ID) {
            arrayOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
        } else {
            arrayOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
        }
        requestPermissions(this, requestCode, *permissions)
    }



    private fun initializeSeekBar() {
        seekBar.max = mediaPlayer.seconds

        runnable = Runnable {
            seekBar.progress = mediaPlayer.currentSeconds

            //tvPass.text = "${mediaPlayer.currentSeconds} sec"
            //val diff = mediaPlayer.seconds - mediaPlayer.currentSeconds
            //tvDue.text = "$diff sec"

            handler.postDelayed(runnable, 1000)
        }
        handler.postDelayed(runnable, 1000)
    }
    /*fun getMediaPlayer():MediaPlayer{
        return mediaPlayer
    }*/
}

val MediaPlayer.seconds: Int
    get() {
        return this.duration / 1000
    }

val MediaPlayer.currentSeconds: Int
    get() {
        return this.currentPosition / 1000
    }


