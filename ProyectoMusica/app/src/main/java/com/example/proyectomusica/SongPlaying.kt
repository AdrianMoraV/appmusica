package com.example.proyectomusica

import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_song_playing.*

class SongPlaying : AppCompatActivity() {
    private lateinit var mediaPlayer: MediaPlayer

    private lateinit var runnable: Runnable
    private var handler: Handler = Handler()
    private var pause: Boolean = false
    private var cancionActualIndex =0
    private var songList = ArrayList<Song>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_song_playing)
        mediaPlayer = MediaPlayer()
        /*var bundle = Bundle()
        bundle = intent.extras ?: Bundle()
        var cancionActual = bundle.getInt("cancionActual")
        var songListRecuperada : ArrayList<Song> = bundle.getParcelableArrayList<Song>("array") as ArrayList<Song>*/
        //var songListRecuperada: Song = intent.getParcelableExtra<Song>("array")!!
        //var songListRecuperada:  ArrayList<Song> = intent.getParcelableArrayListExtra<Song>("ARRAY") as ArrayList<Song>
        //var songListRecuperada = intent.getParcelableArrayListExtra("ARRAY") ?: ArrayList<Song>()
        songList = intent.getParcelableArrayListExtra<Song>("ARRAY") as ArrayList<Song>
        val cancionActual = intent.getIntExtra("CANCIONACTUAL",-1)
        Toast.makeText(this, "Cancion: ${cancionActual}", Toast.LENGTH_SHORT).show()
        cancionActualIndex = cancionActual
        //songList.addAll(songListRecuperada)
        //imgThumbnailPlaying.setImageURI(Uri.parse(songList[cancionActualIndex].thumbnail))
        playMusic(songList[cancionActualIndex].songLink,songList[cancionActualIndex].title,songList[cancionActualIndex].artist,songList[cancionActualIndex].thumbnail)
        val progreso = intent.getIntExtra("PROGRESO", -1)

        imgPlay2.visibility = View.INVISIBLE
        initializeSeekBar()
        mediaPlayer.seekTo(progreso * 1000)

        imgPlay2.setOnClickListener {
            mediaPlayer.start()
            pause = false
            imgPlay2.visibility = View.INVISIBLE
            imgPause2.visibility = View.VISIBLE
        }

        imgPause2.setOnClickListener {
            if (mediaPlayer.isPlaying) {
                mediaPlayer.pause()
                pause = true

                Toast.makeText(this, "media pause", Toast.LENGTH_SHORT).show()
                imgPause2.visibility = View.INVISIBLE
                imgPlay2.visibility = View.VISIBLE
            }
        }
        imgNext2.setOnClickListener {
            playNext(cancionActualIndex)
        }
        imgAnterior2.setOnClickListener {
            playAnterior(cancionActualIndex)
        }

        seekBar2.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                if (b) {
                    mediaPlayer.seekTo(i * 1000)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
            }
        })
    }

    fun playMusic(ID: String?, title: String?, artist: String?,thumbnail: String?) {
        if (pause) {
            mediaPlayer.seekTo(mediaPlayer.currentPosition)
            mediaPlayer.start()
            pause = false
            //Toast.makeText(this, "media playing", Toast.LENGTH_SHORT).show()
            imgPlay2.visibility = View.INVISIBLE
            imgPause2.visibility = View.VISIBLE

        } else {
            mediaPlayer.reset()
            mediaPlayer.apply {
                setDataSource(applicationContext, Uri.parse(ID))
                prepare()
                start()
            }
            /*mediaPlayer =
                MediaPlayer.create(applicationContext, ID)
            mediaPlayer.start()*/
            //Toast.makeText(this, "media playing $ID", Toast.LENGTH_SHORT).show()
            imgPlay2.visibility = View.INVISIBLE
            imgPause2.visibility = View.VISIBLE
            tvNombre2.text = "${title}"
            tvArtista2.text= "${artist}"
            imgThumbnailPlaying.setImageURI(Uri.parse(thumbnail))
        }
        //initializeSeekBar()


        mediaPlayer.setOnCompletionListener {
            Toast.makeText(this, "end", Toast.LENGTH_SHORT).show()
        }

    }
    fun playNext(pos: Int){
        if(pos == songList.size-1){
            var cancion: Song = songList.get(0)
            playMusic(cancion.songLink,cancion.title,cancion.artist,cancion.thumbnail)
            cancionActualIndex=0
        }else{
            var cancion: Song = songList.get(pos+1)
            playMusic(cancion.songLink,cancion.title,cancion.artist,cancion.thumbnail)
            cancionActualIndex=pos+1
        }

    }

    fun playAnterior(pos: Int){
        if(pos == 0){
            var cancion: Song = songList.get(songList.size-1)
            playMusic(cancion.songLink,cancion.title,cancion.artist,cancion.thumbnail)
            cancionActualIndex=songList.size-1
        }else{
            var cancion: Song = songList.get(pos-1)
            playMusic(cancion.songLink,cancion.title,cancion.artist,cancion.thumbnail)
            cancionActualIndex=pos-1
        }

    }
    private fun initializeSeekBar() {
        seekBar2.max = mediaPlayer.seconds

        runnable = Runnable {
            seekBar2.progress = mediaPlayer.currentSeconds

            tvPass2.text = "${mediaPlayer.currentSeconds} sec"
            val diff = mediaPlayer.seconds - mediaPlayer.currentSeconds
            tvDue2.text = "$diff sec"

            handler.postDelayed(runnable, 1000)
        }
        handler.postDelayed(runnable, 1000)
    }
}


