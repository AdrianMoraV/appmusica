package com.example.proyectomusica

import android.media.MediaPlayer
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_song.view.*

class SongAdapter(private val listener: (Song,Int) -> Unit):
RecyclerView.Adapter<SongAdapterViewHolder>(){

    private val lista = mutableListOf<Song>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongAdapterViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_song, parent, false)
        return SongAdapterViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: SongAdapterViewHolder, position: Int) {
        holder.setData(lista[position], position, listener)
    }

    override fun getItemCount(): Int {
        return lista.size
    }

    fun setList(list: List<Song>){
        this.lista.addAll(list)
        notifyDataSetChanged()
    }
}
class SongAdapterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    fun setData(
        item: Song,
        position: Int,
        listener: (Song, Int) -> Unit
    ) {
        itemView.apply {
            tvCancion.text = "${item.title}"
            tvArtista.text = "${item.artist}"
            imgThumbnail.setImageURI(Uri.parse(item.thumbnail))
            setOnClickListener {
                listener.invoke(item, position)
            }
        }
    }
}

